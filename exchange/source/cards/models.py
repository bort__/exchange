"""."""

import datetime
from django.db import models
from order.models import Order
from django.db.models import Sum


class Card(models.Model):
    """."""

    phone = models.CharField(max_length=16)
    number = models.CharField(max_length=16)
    password = models.CharField(max_length=64)
    balance = models.FloatField()
    limit = models.FloatField()
    day_limit = models.FloatField()
    month_limit = models.FloatField()
    operator = models.ForeignKey("user.user", on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    is_closable = models.BooleanField(default=True)
    is_exchangeable = models.BooleanField(default=True)

    def __str__(self):
        """."""
        return "%s: %s" % (self.operator.email, self.number)

    def is_available(self):
        u"""Проверка, что карту можно пустить в работу."""
        start_date = datetime.now().strftime("%Y-%m-%d 00:00:00")
        end_date = datetime.now().strftime("%Y-%m-%d 23:59:59")
        day_limit = Order.objects.filter(operator=self, created__gte=start_date, created__lte=end_date, status__in=["confirmed", "done"]).aggregate(
            day_limit=Sum("amount")
        )["day_limit"]

        start_date = datetime.datetime(day=1, month=datetime.datetime.now().month, year=datetime.datetime.now().year).strftime("%Y-%m-%d 00:00:00")
        end_date = datetime.datetime(day=1, month=datetime.datetime.now().month + 1, year=datetime.datetime.now().year).strftime("%Y-%m-%d 00:00:00")
        month_limit = Order.objects.filter(operator=self, created__gte=start_date, created__lt=end_date).aggregate(
            month_limit=Sum("amount")
        )["month_limit"]

        if day_limit >= self.day_limit or month_limit > self.month_limit:
            return False
        else:
            return True

    def get_current_balance(self):
        u"""Текущий баланс с учетом открытых заяков."""
        orders_amount = Order.objects.filter(operator=self).exclude(status__in=["rejected", "expired"]).aggregate(
            orders_amount=Sum("amount")
        )["orders_amount"]

        return orders_amount + self.balance

    def get_day_balances(self):
        pass

    def get_month_balance(self):
        pass

    class Meta:
        """."""

        db_table = "card"
