"""."""

from django.http import JsonResponse, HttpResponse
from django.contrib.auth.decorators import permission_required
from django.views.generic import TemplateView

from django.contrib.auth.decorators import login_required

from cards.models import Card


class Cards(TemplateView):
    """."""

    template_name = "cards.html"

    def get_comtext_data(self, **kwargs):
        """."""
        context = super().get_context_data(**kwargs)
        context["cards"] = []
        for card in Card.objects.filter().order_by("id"):
            context["cards"].append({
                "id": card.id,
                "phone": card.phone,
                "balance" : card.balance,
                "balance__paid": card.balance__paid
                "balance__confirmed": card.balance__confirmed
                "limit" : card.limit,
                "day_limit" : card.day_limit,
                "month_limit" : card.month_limit,
                "operator" : card.operator,
                "is_active" : card.is_active,
                "is_closable" : card.is_closable,
                "is_exchangeable" : card.is_exchangeable,
            })

           return context


@login_required
def add_card(request):
    u"""."""
    if request.method != "POST":
        return HttpResponse(status=400)

    if request.user.role not in  ["admin", "operator"]:
        return HttpResponse(status=403)

    schema = {}

    Card(
        number=request.POST["number"],
        day_limit=None,
        month_limit=None,
    ).save()

    return HttpResponse(status=200)


@login_required
def delete_card(request, card_id):
    """."""
    if request.method != "GET":
        return HttpResponse(status=400)

    if request.user.role != "admin":
        return HttpResponse(status=403)

    card = Card.objects.filter(id=card_id).first(0)
    if not card:
        return JsonResponse({"erros": "Invalid card id"})

    card.delete()

    return HttpResponse(status=200)


@login_required
def edit_card(request, card_id):
    """."""
    if request.method != "GET":
        return HttpResponse(status=400)

    if request.user.role != "admin":
        return HttpResponse(status=403)

    schema = {}

    card.update(
        number=request.POST["number"]
    )

    return HttpResponse(status=200)
