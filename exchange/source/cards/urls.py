"""exchange URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
import views

urlpatterns = [
    path("cards/", views.orders),
    path("cards/add", views.orders),
    path("card/<int: card_id>/edit", views.orders),
    path("card/<int: card_id>/delete", views.orders),

    path("sms", views.sms),
    path("qiwi", views.qiwi),

    path("order/<int: order_id>/edit", views.edit_order),
    path("order/<int: order_id>/delete", views.delete_order),
]
