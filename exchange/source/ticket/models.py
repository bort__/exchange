from django.db import models


ticket_statuses = (
    (1, "open"),
    (2, "close")
)


class Ticket(models.Model):
    """Тикеты по проблемным заявкам."""

    order = models.ForeignKey("order.Order", on_delete=models.CASCADE)
    operator = models.ForeignKey("user.user", on_delete=models.CASCADE)
    description = models.CharField(max_length=1024)
    status = models.CharField(max_length=16, choices=ticket_statuses)

    class Meta:
        """."""

        db_table = "ticket"
