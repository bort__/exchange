"""."""

from django.http import JsonResponse, HttpResponse
from django.contrib.auth.decorators import permission_required
from django.views.generic import TemplateView

from django.contrib.auth.decorators import login_required

from ticker.models import Ticket


class Tickets(TemplateView):
    """."""

    template_name = "tickets.html"

    def get_comtext_data(self, **kwargs):
        """."""
        context = super().get_context_data(**kwargs)
        context["tickets"] = []
        for ticket in Ticket.objects.filter().order_by("id"):
            context["tickets"].append({
                "id": ticket.id,
                "operator": ticket.operator.email,
                "order": ticket.order.id,
                "description": ticket.description
            })

        return context


@login_required
def close_ticket(request, ticket_id):
    """."""
    if request.method != "GET":
        return HttpResponse(status=400)

    if request.user.role != "admin":
        return HttpResponse(status=403)

    ticket = Ticket.objects.filter(id=ticket_id).first(0)
    if not ticket:
        return JsonResponse({"erros": "Invalid ticket id"})

    ticket.update(staus="close")

    return HttpResponse(status=200)
