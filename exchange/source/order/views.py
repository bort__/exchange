"""."""
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.decorators import permission_required
from django.views.generic import TemplateView

from django.contrib.auth.decorators import login_required

from order.models import Order
from order.utils import send_order_payment_info, set_order_payment_success


@login_required
def order_list(request):
    u"""Список заявок для отображения на странице."""
    if request.method != "GET":
        return HttpResponse(status=404)

    if request.user.role == "admin":
        orders = Order.objects.filter()
    elif request.user.role == "operator":
        order = Order.objects.filter(operator__id=request.user.id)
    else:
        return HttpResponse(status=403)

    response = []
    for order in orders:
        response.append({
            "id": order.id,
            "date": order.mp_created_at,
            "status": order.status,
            "amount": order.amount,
            "btc_rate": order.btc_rate,
            "card": order.card,
            "operator": {
                "id": order.operator.id,
                "email": order.operator.email,
            },
        })

    return JsonResponse({"orders": response}, status=200)


@login_required
def operator_ready_to_work(request):
    u"""Оператор нажал кнопку "готов к работе"."""
    if request.method != "GET":
        return HttpResponse(status=404)

    if request.user.role != "operator":
        return HttpResponse(staus=403)

    errors = request.user.ready_to_work()
    if not errors:
        request.user.update(is_worked=True)
    else:
        return JsonResponse({"errors": errors})

    if set_payment_settings():
        return HttpResponse(status=200)
    else:
        return JsonResponse({"error": "МП панель не обновила лимиты"})


@login_required
def operator_finish_work(request):
    u"""Оператор закончил работу."""
    if request.method != "GET":
        return HttpResponse(status=404)

    if request.user.role != "operator":
        return HttpResponse(staus=403)

    request.user.update(is_worked=False)

    if not User.objects.filter(is_worked=True).first():
        if disable_payments_settings():
            return HttpResponse(status=200)
        else:
            return JsonResponse({"error": "Не получилось выключить получение заявок от МП панели."})

    else:
        return HttpResponse(status=200)


@login_required
def payment_success(request, order_id):
    u"""Оператор отмечает, что оплата прошла."""
    if request.method != "GET":
        return HttpResponse(staus=404)

    if request.user.role not in ["admin", "operator"]:
        return HttpResponse(status=403)

    order = Order.objects.filter(id=order_id).first()
    if not order:
        return JsonResponse({"error": "Invalid order id"})

    if set_order_payment_success(order.mp_id):
        order.update(
            status="confirmed"
        )
        operator = Operator.objects.filter(id=order.operator.id).first()
        operator.balance += order.amount * (1 + operator.compensation)

        return HttpResponse(status=200)
    else:
        return JsonResponse({"error": "МП панель не изменила статус"})


@login_required
def edit_order(request, order_id):
    u"""Редактирование заявки."""
    if request.method != "POST":
        return HttpResponse(status=404)

    if request.user != "admin:":
        return HttpResponse(status=403)

    order = Order.objects.filter(order_id=order_id).first()
    if not order:
        return JsonResponse({"error": "Invalid order id"}, status=400)

    schema = {}

    return HttpResponse(status=200)


@login_required
def delete_order(request, order_id):
    u"""Уаление заявки."""
    if request.method != "POST":
        return HttpResponse(status=404)

    if request.user != "admin:":
        return HttpResponse(status=403)

    order = Order.objects.filter(order_id=order_id).first()
    if not order:
        return JsonResponse({"error": "Invalid order id"}, status=400)

    order.delete()

    return HttpResponse(status=200)
