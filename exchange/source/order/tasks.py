u"""."""
import requests
import json
from order.models import Order
from ticket.models import Ticket
from exchange.celery import app
from .utils import find_order_operator, notify_admin, notify_operator
from datetime import datetime, timedelta
from exchange.settings import MP_URL, MP_LOGIN, MP_PASSWORD, MP_TOKEN


@app.task(name="exchange.order.mp_orders")
def mp_orders():
    u"""Лезем на мп панель за новыми заявками."""
    print("MP ORDERS")
    print(MP_URL)
    # btc_rate = json.loads(requests.get("https://blockchain.info/ticker").content)["RUB"]["last"]

    # response = requests.get(
    #     MP_URL + "/orders/new/",
    #     auth=requests.auth.HTTPBasicAuth(login, password),
    #     headers={"Token": token},
    # )

    # for order in response["results"]:
    #     if order["payment_system"]["id"] != payment_system.id and \
    #        order["payment_system"]["name"] != payment_system.name:
    #         notify_admin()

    #     # Првоерка курса????

    #     Order(
    #         mp_id=order["id"],
    #         mp_status=order["status"],
    #         mp_created=order["created_at"],
    #         amount=order["amount"],
    #         status="open",
    #     ).save()

    # find_order_operator()


@app.task(name="exchange.order.payment_check")
def payment_check():
    u"""Проверяем, что оплата по заявке прошла."""
    print("PAYMENT CHECK")
    # for order in Order.objects.filter(status="pinfo"):
    #     response = requests(
    #         mp_url + "/orders/all/%s/" % order.mp_id,
    #         auth=requests.auth.HTTPBasicAuth(login, password),
    #         headers={"Token": token},
    #     )

        # if response["body"]["status"] == 1:
        #     order.status = "paid"
        #     order.save()
        #     notify_operator()


@app.task(name="exchange.order.send_btc")
def repay_mp_panel():
    u"""Возвращаем мп панели биткоины."""
    orders = Order.objects.filter(status="confirmed")

    amount_btc = orders.aggregate(
        amount=Sum("amount")
    )

    if amount_btc > Settings.objects.filter(id=1).first()["amount_btc"]:
        body = {
            "orders": []
        }

        for order in orders:
            body["orders"].append({"id": order.id, "mpid": order.mp_id})

        requests.get("/vanya_wallet_url")
        orders.update(status="done")


@app.task(name="exchange.order.expired_order")
def find_expired_orders():
    u"""Ищем сломанные заявки.

    Заяки сломаные, если за время жизни заявки не случилось следущее:
        1) Оператор не ввел карту для оплаты
        2) Истекло время ожидания оплаты от клиента
        3) Оплачена неверная сумма
    """
    expired_time = datetime.now() - timedelta(minutes=Settings.objects.first(id=1).first()["order_expired"])
    for order in Order.objects.filter(created_at__lt=expired_time, status__in=["open", "pinfo", "paid"]):
        if order.status == "open":
            Ticket(
                order=order,
                operator=order.operator,
                description="Оператор %s не указал номер карты для оплаты у заявки %s" % (order.operator.email, order.id)
            ).save()

            order.expire()

        if order.status == "pinfo":
            Ticket(
                order=order,
                operator=order.operator,
                description="Клиент не заплатил по заявке %s" % (order.id)
            ).save()

            order.reject()

        if order.status == "paid":
            Ticket(
                order=order,
                operator=order.operator,
                description="Оператор %s не подтвердил оплату заявки %s" % (order.id)
            ).save()

            order.expire()
