import json

from channels.generic.websocket import AsyncWebsocketConsumer, WebsocketConsumer
from channels.consumer import AsyncConsumer
from . import tasks
import time
import sys
import json
# class ChatConsumer(WebsocketConsumer):
#     def connect(self):
#         self.accept()
#         print("CONNECT")

#     def disconnect(self, close_code):
#         print("DISCONNECT %s" % close_code)

#     def receive(self, text_data):
#         # Here we receive some data from the client
#         text_data_json = json.loads(text_data)
#         message = text_data_json['message']
#         print("GET WS !!!!")
#         print(message)

        
#         # Here we can process client data and send result back directly
#         # to the client (by using his unique channel name - `self.channel_name`)
        
#         time.sleep(100)
      
#         # And send some result back to that client immediately
#         self.send(text_data=json.dumps({'message': 'Your request was received!'}))

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync


class Consumer(WebsocketConsumer):

    def websocket_connect(self, event):
        """."""
        async_to_sync(self.channel_layer.group_add)(
            'operator',
            self.channel_name
        )

        self.accept()

    def websocket_disconnect(self, event):
        """."""
        async_to_sync(self.channel_layer.group_discard)(
            "operator",
            self.channel_name
        )
        self.close()

    def send_message(self, data):
        """."""
        print(data)
        sys.stdout.flush()
        self.send(json.dumps({
            "action": data["data"]["action"],
            "data": data,
        }))


def send_msg(message):
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        'operator',
        {
            'type': 'send.message',
            "data": {"action": "JSUT DO IT", "operator": 1, "orders": []}
        }
    )
