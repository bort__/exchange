from django.db import models

orders_statuses = (
    (1, "open"),
    (2, "pinfo"),
    (3, "paid"),
    (4, "confirmed"),
    (5, "done"),
    (6, "rejected"),
    (7, "expired"),

)


class Order(models.Model):
    """."""

    mp_id = models.IntegerField(blank=False, null=True)
    mp_status = models.CharField(max_length=128, null=True)
    mp_created = models.DateTimeField(blank=False, null=True)
    amount = models.FloatField(null=True, blank=True)
    status = models.CharField(max_length=128, choices=orders_statuses, default="open")
    operator = models.ForeignKey("user.user", null=True, on_delete=models.PROTECT)

    def __str__(self):
        """."""
        return "%s: %s" % (self.id, self.amount)

    def move_status(self):
        """."""
        if self.status[0] < 5:
            self.status += 1

    def expire(self):
        """."""
        self.status = "expired"

    def reject(self):
        """."""
        self.status = "rejected"

    class Meta:
        """."""

        db_table = "orders"
