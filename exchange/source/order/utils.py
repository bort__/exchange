"""."""

import requests
from django.db.models import Max, Min, Sum
from user.models import User
from order.models import Order
from cards.models import Card
from exchange.settings import MP_URL, MP_LOGIN, MP_PASSWORD, MP_TOKEN, PAYMENT_SYSTEM


def notify_admin():
    """."""
    pass

def notify_operator():
    pass


def notify_operator_new_orders():
    """."""
    pass


def find_order_operator():
    u"""Поиск оператора для заявки.

    Гавно и переделать
    Бесконечный цикл и уведомление о нерозданых заявках
    """
    operators = Operator.objects.filter(is_worked=True).exclude(coefficient=0)
    coefficient = operators.aggregate(
        weight=Sum("coefficient")
    )["coefficient"]

    orders = Order.objects.filter(status="open", operator=None)

    coefficient = orders.count() / coefficient

    for operator in operators:
        assigned_orders = 0
        cards = Card.objects.filter(operator=operators)
        while assigned_orders < round(operator.coefficient * coefficient):
            for order in orders:
                available_cards = list(filter(lambda c: c.is_available, cards))

                if available_cards:
                    order.payment_info = available_cards[0].number
                    order.operator = operator

                    assigned_orders += 0

    for order in orders:
        if send_order_payment_info(order.mp_id, order.payment_info):
            order.update(status="pinfo")

    notifi_operator_new_orders()
    ## сообщить о новых заявках


def set_payment_settings():
    u"""Перед началом работы нужно отправить в мп панель лимиты и платежную систему."""
    limits = Operator.objects.filter(is_worked=True).aggregate(
        max_limit=Max("max_limit"),
        min_limit=Min("min_limit")
    )

    request_body = {
        "payment_system": PAYMENT_SYSTEM,
        "trades_enabled": True,
        "min_amount": limits["min_limit"],
        "max_amount": limits["max_limit"]
    }

    response = requests.get(
        MP_URL + "/settings/payment_settings/{id}/",
        auth=requests.auth.HTTPBasicAuth(MP_LOGIN, MP_PASSWORD),
        headers={"Token": MP_TOKEN},
        body=request_body
    )

    if response["status"] == 200:
        return True
    else:
        return False


def disable_payments_settings():
    u"""Выключаем раздачу заявок, чтобы мп панель не обижалась."""
    response = requests.post(
        MP_URL + "/settings/payment_settings/{id}/",
        auth=requests.auth.HTTPBasicAuth(MP_LOGIN, MP_PASSWORD),
        headers={"Token": MP_TOKEN},
        body={"trades_enabled": False}
    )

    if response.status == 200:
        return True
    else:
        return False


def send_order_payment_info(order_mpid, card):
    u"""Посылаем в мп панель номер карты для оплаты."""
    response = requests.post(
        MP_URL + "/settings/payment_settings/%s/" % order_mpid,
        auth=requests.auth.HTTPBasicAuth(MP_LOGIN, MP_PASSWORD),
        headers={"Token": MP_TOKEN},
        body={"payment_to": card}
    )

    if response["status"] == 200:
        return True
    else:
        return False


def set_order_payment_success(order_mpid, amount):
    u"""Говорим мп панели, что оператор получил деньги."""
    response = requests.put(
        MP_URL + "/orders/set_success/%s/" % order_mpid,
        auth=requests.auth.HTTPBasicAuth(MP_LOGIN, MP_PASSWORD),
        headers={"Token": MP_TOKEN},
        body={"amount": amount}
    )

    if response["status"] == 200:
        return True
    else:
        return False
