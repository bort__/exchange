"""."""

from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.auth.models import UserManager
from django.db import models

# from wallet.models import WalletHistory

roles = (
    ("Admin", "All privileges"),
    ("Support", "Work with tickets"),
    ("Operator", "Work with orders")
)


class Token(models.Model):
    u"""Токен, использующийся для самостоятельной регистрации пользователя.

    Предопределяет роль будущего пользователя.
    """

    created = models.DateTimeField(auto_now_add=True)
    used = models.BooleanField(default=False)
    role = models.CharField(max_length=16, choices=roles)
    token = models.CharField(max_length=64)
    # refferer = models.ForeignKey("user.User", blank=False, null=True, on_delete=models.SET_NULL)
    refferal = models.ForeignKey("user.User", blank=False, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        """."""
        return "%s: %s" % (self.role, self.token)

    class Meta:
        """."""

        db_table = "token"


class User(AbstractBaseUser, PermissionsMixin):
    u"""."""

    created = models.DateTimeField(auto_now_add=True)

    email = models.EmailField(unique=True)
    balance = models.FloatField(default=0)
    credit_limit = models.FloatField(default=0)
    btc_address = models.CharField(max_length=64)
    role = models.CharField(max_length=32, blank=True, null=True)
    chat_id = models.CharField(max_length=64, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_stoped = models.BooleanField(default=True)

    # refferer = models.ForeignKey("user.User", on_delete=models.PROTECT)

    USERNAME_FIELD = "email"
    EMAIL_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = UserManager()

    def change_btc_address(self, btc_address, last_txid, confirmations):
        u"""Изменение адреса кошелька с занесением в историю."""
        WalletHistory(
            operator=self,
            btc_address=self.btc_address,
            txid=last_txid,
            confirmation=confirmations,
        ).save()

        self.btc_address = btc_address

    class Meta:
        """."""

        db_table = "user"

        permissions = [
            ("admin", "Имеет все права"),
            ("operator", "Работает с заявками"),
            ("support", "Работает с тикетами"),
        ]
