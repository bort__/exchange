"""."""

from django.http import JsonResponse, HttpResponse
from django.contrib.auth.decorators import permission_required
from django.views.generic import TemplateView

from django.contrib.auth.decorators import login_required

from models import Token, User
from utils import generate_token


class UserPanel(TemplateView):
    """."""

    template_name = "user_panel.html"

    def get_context_data(self, **kwargs):
        """."""
        pass


@permission_required("admin")
def direct_registration(request):
    u"""Регистрация админом из панели."""
    if request.method != "POST":
        return HttpResponse(status=404)

    schema = {
        "email": "Укажите почту",
        "password": "",
        "balance": "",
        "credit_limit": "",
        "btc_address": "",
        "chat_id": "",
        "role": "",
        "is_active": "",
        "is_stoped": "",
    }

    errors = []
    for key in schema.keys():
        if not request.get(key):
            errors.append(schema[key])
    if errors:
        return JsonResponse({"errors": errors}, status=400)

    user = User(
        email=request.POST["email"],
        balance=request.POST.get("balance", 0),
        limit=request.POST.get("limit", 0),
        btc_address=request.POST.get("btc_address", None),
        chat_id=request.POST.get("chat_id", None),
        role=request.POST.get("role", "operator"),
        is_active=request.POST.get("is_active", True),
        is_stoped=request.POST.get("is_stoped", False),
        referrer=request.user,
    )
    user.set_password(request.POST["password"])
    user.save()

    return HttpResponse(status=200)


def token_reqistration(request):
    u"""Пользователь сам регистрируется по выданому токену."""
    if request.method != "POST":
        return HttpResponse(status=404)

    schema = {
        "email": "Укажите почту",
        "password": "Укажите пароль",
        "token": "Необходим токен",
    }

    errors = []
    for key in schema.keys():
        if not request.get(key):
            errors.append(schema[key])
    if errors:
        return JsonResponse({"errors": errors}, status=400)

    token = Token.objects.filter(token=request.POST["token"]).first()
    if not token:
        return JsonResponse({"error": "Invalid token"})

    user = User(
        email=request.POST["email"],
        role=token.role,
        limit=0,
        btc_address=None,
        chat_id=None,
        is_active=True,
        is_stoped=False,
        referrer=token.referrer
    )
    user.set_password(request.POST["password"])
    user.save()

    token.update(
        refferal=user,
        used=True,
    )

    return HttpResponse(status=200)


def create_invite_token(request):
    u"""Создание инвайт токена.

    Оператор может только запросить токен с ролью "опреатор".
    Админ может выбрать роль.
    """
    if request.method == "GET" and request.user.role in ["operator"]:
        role = "operator"

    else:
        return JsonResponse({"error": "Недостаточно прав"}, status=400)

    if request.method == "POST":

        if request.user.role not in ["admin"]:
            return JsonResponse({"error": "Недостаточно прав"}, status=400)

        if not request.POST.get("role"):
            return JsonResponse({"error": "Нужно указать роль"}, status=400)

        role = request.POST["role"]

    token = generate_token()

    Token(
        role=role,
        referrer=request.user,
        referral=None,
        token=token,
    ).save()

    return JsonResponse({"token": token}, status=200)


@permission_required("admin")
def users_list(request):
    u"""Список всех пользователей для отображения на странице."""
    if request.method != "GET":
        return HttpResponse(status=404)

    users = []
    for user in User.objects.all():
        users.append({
            "id": user.id,
            "active": user.active,
            "stop": user.stop,
            "email": user.email,
            "balance": user.balance,
            "limit": user.limit,
            "timetable": user.timetable,
            "btc_address": user.btc_address,
            "role": user.role,
        })

    return JsonResponse({"users": users}, status=200)


def edit_user(request, user_id):
    u"""Изменение настроек юзера.

    Админ может менять все.
    Реферрер пользователя может менять только расписание.
    """
    if request.method != "POST":
        return HttpResponse(status=404)

    if request.user.role not in ["admin", "operator"]:
        return JsonResponse({"error": "Недостаточно прав"}, status=400)

    user = User.objects.filter(id=user_id).first()
    if not user:
        return JsonResponse({"error": "Invalid user id"}, status=400)

    if request.user.role != "admin" or user.referrer != request.user:
        return JsonResponse({"error": "Менять настройки пользователя может только пригласивший"}, status=400)

    schema = {

    }
    errors = []
    for key in schema.keys():
        if not request.get(key):
            errors.append(schema[key])
    if errors:
        return JsonResponse({"errors": errors}, status=400)

    user.update(
        email=request.POST["email"],
        balance=request.POST["balance"],
        limit=request.POST["limit"],
        compensation=request.POST["compensation"],
        btc_address=request.POST["btc_address"],
        chat_id=request.POST["chat_id"],
        role=request.POST["role"],
    )

    return HttpResponse(status=200)


@permission_required("admin")
def delete_user(request, user_id):
    """."""
    if request.method != "GET":
        return HttpResponse(status=404)

    user = User.objects.filter(user_id).first()
    if not user:
        return JsonResponse({"error": "Invalid user id"}, status=400)

    user.delete()

    return HttpResponse(status=200)
