"""."""

from models import Token
from random import choice

ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!#$%&~^*()+=-"


def generate_token():
    u"""Генерация токена для регистрации.

    Потом переделать под реферерра и выдаваемую роль.
    Опасно: рекурсия может выйти из под контроля!
    """
    token = "".join(choice(ALPHABET) for x in range(24))

    if Token.objects.filter(token=token).first():
        token = generate_token()

    return token
