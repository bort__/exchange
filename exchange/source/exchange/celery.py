u"""Задачи сельдерея по заявкам."""

import os
from celery import Celery

# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'exchange.settings')

app = Celery('exchange', broker="redis://redis:6379", include=["order.tasks"])
app.config_from_object('django.conf:settings', namespace='celery')
app.autodiscover_tasks()


@app.task(name="exchange.exchange.ping")
def pingpong():
    """."""
    print("\nHELLO!!!!\n")


app.conf.beat_schedule = {
    "helloworld": {
        "task": "exchange.exchange.ping",
        "schedule": 60
    },
    "mp_new_orders": {
        "task": "exchange.order.mp_orders",
        "schedule": 60
    },
    "mp_check_payment": {
        "task": "exchange.order.payment_check",
        "schedule": 60
    }
}
