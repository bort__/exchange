from channels.routing import ProtocolTypeRouter, URLRouter
import order.routing

application = ProtocolTypeRouter({
    # (http->django views is added by default)
    'websocket': URLRouter(
        order.routing.websocket_urlpatterns
    ),
})
